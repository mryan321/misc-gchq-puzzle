#!/usr/bin/env python

from itertools import product

# Take the digits 1,2,3 up to 9 in numerical order and put either a plus sign or a minus sign or neither between the
# digits to make a sum that adds up to 100. For example, one way of achieving this is:
#
# 1 + 2 + 34 - 5 + 67 - 8 + 9 = 100 - which uses six plusses and minuses.
#
# What is the fewest number of plusses and minuses you need to do this?
#
# From BBC Today URL:
# The answer is three: 123 - 45 - 67 + 89 = 100.
# There are 11 ways in total of completing the sum if you don't restrict the number of plusses and minuses allowed.


def get_next_number(numbers, startIndex, combination, lastNumber):
    for index in range(startIndex, len(numbers)):
        number = numbers[index]
        operator = None if index is len(combination) else combination[index]
        if operator is '.' or operator is None:
            newNumber = int(str(lastNumber) + str(number))
            if operator is None:
                return newNumber
            else:
                return get_next_number(numbers, startIndex+1, combination, newNumber)
        elif lastNumber > 0:
            newNumber = int(str(lastNumber) + str(number))
            return newNumber
        else:
            return number


if __name__ == '__main__':

    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    maxOperatorCount = len(numbers) - 1
    plusMinusCount = 0

    print 'Starting GCHQ puzzle:'
    print '>> numbers = %s ::: max operator count = [%d]' % (numbers, maxOperatorCount)

    plusMinusHits = []
    lowestPlusMinusCount = maxOperatorCount + 1

    # calculate possible +/- combinations (before ordering taken into account)
    # +=plus, -=minus, .=join
    allCombinations = list(product(['+', '-', '.'], repeat=maxOperatorCount))
    print '>> permutations ::: %s' % allCombinations

    for combination in allCombinations:
        numbersForMath = []
        index = 0
        while index < len(numbers):
            number = numbers[index]
            nextNumber = get_next_number(numbers, index, combination, 0)
            index += len(str(nextNumber))  # in case of recursion
            numbersForMath.append(nextNumber)

        # do the math
        combinationForMath = filter(lambda c: c is not '.', combination)
        operator = None
        lastNumber = 0
        total = 0
        for index in range(len(numbersForMath)):
            number = numbersForMath[index]

            if operator is None:
                operator = operator if index is len(combinationForMath) else combinationForMath[index]
                total = number
                continue

            total = total+number if operator is '+' else total-number
            operator = operator if index is len(combinationForMath) else combinationForMath[index]

        if total is 100:
            print '>> %s ::: %d' % (combination, total)
            plusMinusHits.append(combination)
            if len(combinationForMath) < lowestPlusMinusCount:
                lowestPlusMinusCount = len(combinationForMath)

    print 'Total Hits = %d ::: Fewest plusses and minuses = %d' % (len(plusMinusHits), lowestPlusMinusCount)
